# Multipage Events App - React App (Platform Electives Objective)
A simple React App that allows users view all the listed events available which are fetched through a local database server. These events in summary shows the title of the event as well as the event image and event date. Users could click the event to display the details as well as the ability to edit and delete the event. Users could also add new events through a form similar to editing its data. The data of the events are stored locally through a json file and would be accessed even through server reboot. Navigation and React Routing is showcased all throughout the pages of the application as well as the calling components even though the application is deemed such as a single page application. The main objective of this app was using React Router and its hooks.

## Setup
**Backend**

```
cd backend
npm install
npm run start
```

**Frontend**

```
cd frontend
npm install
npm run start
```
